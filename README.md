# AA k-mers Distribution #

Each csv in this repository contains the distribution of k-mer length AA from different cell types, either B or T cell.

### Repository Files ###

* TCRB.kmers.csv        -  T cell receptor beta chain k-mer distribution
* IGHPBMC.kmers.csv     -  B cell receptor heavy chain from PBMCs k-mer distribution
* IGHNaive.kmers.csv    -  B cell receptor heavy chain from Naive k-mer distribution
* lightPBMC.kmers.csv   -  B cell receptor light chain from PBMCs k-mer distribution
* lightNaive.kmers.csv  -  B cell receptor light chain from Naive k-mer distribution


